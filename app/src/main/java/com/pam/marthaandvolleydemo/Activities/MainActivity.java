package com.pam.marthaandvolleydemo.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pam.marthaandvolleydemo.Model.Item;
import com.pam.marthaandvolleydemo.R;
import com.pam.marthaandvolleydemo.Requests.Items.DeleteItemRequest;
import com.pam.marthaandvolleydemo.Requests.Items.InsertItemRequest;
import com.pam.marthaandvolleydemo.Requests.MarthaRequest;
import com.pam.marthaandvolleydemo.Requests.MarthaRequestQueue;
import com.pam.marthaandvolleydemo.Requests.Items.SelectItemsRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addButton = (Button)findViewById(R.id.main_button_add);
        addButton.setOnClickListener(onAddButtonClickListener);

        findViewById(R.id.main_button_refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reloadData();
            }
        });

        ListView listView = (ListView) findViewById(R.id.main_listview_items);
        listView.setOnItemLongClickListener(onItemLongCLickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

        reloadData();
    }

    private View.OnClickListener onAddButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final EditText nameEditText = (EditText)findViewById(R.id.main_editText_name);
            String itemName = nameEditText.getText().toString().trim();

            if (itemName.isEmpty()) {
                Toast.makeText(MainActivity.this, "Specify a name!", Toast.LENGTH_LONG).show();
            } else {
                Item item = new Item(itemName);

                MarthaRequest insertRequest = new InsertItemRequest(item, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);

                            if (json.getBoolean("success")) {
                                String message = "Inserted item";

                                if (json.has("lastInsertId")) {
                                    message += (" #" + json.getInt("lastInsertId"));
                                }

                                nameEditText.setText("");
                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();

                                reloadData();
                            } else {
                                Toast.makeText(MainActivity.this, "Could not insert: " + json.getString("error"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Could not parse response: " + response, Toast.LENGTH_LONG).show();
                        }

                        Log.d("DD", response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Could not complete request: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                        Log.d("DD", error.getLocalizedMessage());
                    }
                });

                MarthaRequestQueue.getInstance(MainActivity.this).add(insertRequest);
            }
        }
    };

    AdapterView.OnItemLongClickListener onItemLongCLickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            Item item = (Item) parent.getAdapter().getItem(position);

            MarthaRequest deleteRequest = new DeleteItemRequest(item, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject json = new JSONObject(response);

                        if (json.getBoolean("success")) {
                            Toast.makeText(MainActivity.this, "Delete succeeded! :)", Toast.LENGTH_SHORT).show();

                            reloadData();
                        } else {
                            Toast.makeText(MainActivity.this, "Could not delete: " + json.getString("error"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(MainActivity.this, "Could not parse response: " + response, Toast.LENGTH_LONG).show();
                    }

                    Log.d("DD", response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(MainActivity.this, "Could not complete request: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    Log.d("DD", error.getLocalizedMessage());
                }
            });

            MarthaRequestQueue.getInstance(MainActivity.this).add(deleteRequest);

            return true;
        }
    };

    private void reloadData() {
        MarthaRequest selectRequest = new SelectItemsRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);

                    if (json.getBoolean("success")) {

                        JSONArray data = json.getJSONArray("data");

                        ArrayList<Item> items = new ArrayList<>();
                        Gson gson = new GsonBuilder().create();

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject itemJson = data.getJSONObject(i);

                            items.add(gson.fromJson(itemJson.toString(), Item.class));
                        }

                        ItemsArrayAdapter itemsAdapter = new ItemsArrayAdapter(MainActivity.this, items);
                        ListView listView = (ListView) findViewById(R.id.main_listview_items);
                        listView.setAdapter(itemsAdapter);

                        Toast.makeText(MainActivity.this, "Refresh succeeded! :)", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Could not refresh: " + json.getString("error"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, "Could not parse response: " + response, Toast.LENGTH_LONG).show();
                }

                Log.d("DD", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Could not complete request: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Log.d("DD", error.getLocalizedMessage());
            }
        });

        MarthaRequestQueue.getInstance(MainActivity.this).add(selectRequest);
    }
}
