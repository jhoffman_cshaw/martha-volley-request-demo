package com.pam.marthaandvolleydemo.Requests;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by jhoffman on 2017-04-24.
 */

public class MarthaRequestQueue {
    private static MarthaRequestQueue instance;
    private RequestQueue requestQueue;

    private MarthaRequestQueue(Context context) {
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized MarthaRequestQueue getInstance(Context context) {
        if (instance == null) {
            instance = new MarthaRequestQueue(context);
        }
        return instance;
    }

    public void add(Request request) {
        requestQueue.add(request);
    }
}
