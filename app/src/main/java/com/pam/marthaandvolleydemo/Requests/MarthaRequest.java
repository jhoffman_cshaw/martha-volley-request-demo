package com.pam.marthaandvolleydemo.Requests;

import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jhoffman on 2017-04-24.
 */

public abstract class MarthaRequest extends StringRequest {
    public MarthaRequest(String queryName, Response.Listener listener, Response.ErrorListener errorListener) {
        super(  Request.Method.POST,
                "http://projet-h17.teachingjames.ca/queries/"+ queryName +"/execute",
                listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<>();

        byte[] data = new byte[0];
        try {
            data = "demo_http:demo_http".getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) { /* nothing to do */ }

        String auth = Base64.encodeToString(data, Base64.DEFAULT);
        headers.put("auth", auth);

        return headers;
    }
}
