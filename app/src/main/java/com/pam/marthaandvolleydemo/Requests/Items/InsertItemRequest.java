package com.pam.marthaandvolleydemo.Requests.Items;

import com.android.volley.Response;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pam.marthaandvolleydemo.Model.Item;
import com.pam.marthaandvolleydemo.Requests.MarthaRequest;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;

/**
 * Created by jhoffman on 2017-04-24.
 */

public class InsertItemRequest extends MarthaRequest {
    private Item itemToInsert;

    public InsertItemRequest(Item item, Response.Listener listener, Response.ErrorListener errorListener) {
        super("insert-item", listener, errorListener);

        itemToInsert = item;
    }

    @Override
    public byte[] getBody() {
        byte[] body = new byte[0];

        try {
            GsonBuilder builder = new GsonBuilder();
            builder.setFieldNamingStrategy(new AddClassNameStrategy());

            Gson gson = builder.create();
            String str = gson.toJson(itemToInsert);

            body = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) { /* nothing to do */}

        return body;
    }

    private class AddClassNameStrategy implements FieldNamingStrategy {
        @Override
        public String translateName(Field f) {
            //To prefix JSON fields with their classname, ex: {"Item.id":-1,"Item.name":"ddd"}
            return f.getDeclaringClass().getSimpleName() + "." + f.getName();
        }
    }
}
