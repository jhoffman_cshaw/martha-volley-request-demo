package com.pam.marthaandvolleydemo.Requests.Items;

import com.android.volley.Response;
import com.pam.marthaandvolleydemo.Model.Item;
import com.pam.marthaandvolleydemo.Requests.MarthaRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by jhoffman on 2017-04-24.
 */

public class DeleteItemRequest extends MarthaRequest {
    private Item itemToDelete;

    public DeleteItemRequest(Item item, Response.Listener listener, Response.ErrorListener errorListener) {
        super("delete-item", listener, errorListener);

        itemToDelete = item;
    }

    @Override
    public byte[] getBody() {
        byte[] body = new byte[0];

        try {
            JSONObject json = new JSONObject();
            json.put("id", itemToDelete.id);

            body = json.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
          catch (JSONException e) { e.printStackTrace(); }

        return body;
    }
}
