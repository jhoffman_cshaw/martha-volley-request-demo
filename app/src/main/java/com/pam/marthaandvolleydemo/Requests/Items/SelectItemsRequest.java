package com.pam.marthaandvolleydemo.Requests.Items;

import com.android.volley.Response;
import com.pam.marthaandvolleydemo.Requests.MarthaRequest;

/**
 * Created by jhoffman on 2017-04-24.
 */

public class SelectItemsRequest extends MarthaRequest {
    public SelectItemsRequest(Response.Listener listener, Response.ErrorListener errorListener) {
        super("select-all-items", listener, errorListener);
    }
}
