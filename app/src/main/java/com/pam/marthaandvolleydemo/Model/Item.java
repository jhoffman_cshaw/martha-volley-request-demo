package com.pam.marthaandvolleydemo.Model;

/**
 * Created by jhoffman on 2017-04-24.
 */

public class Item {
    public int id = -1;
    public String name;

    public Item(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "#" + id + " - " + name;
    }
}
